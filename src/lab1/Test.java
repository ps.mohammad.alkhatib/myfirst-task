package lab1;

public class Test {

    public static void main(String[] args) {

        testSum();
        testScale();
        testTransport();
        testMultiply();

        testToDiagonal();
        testToLowerTriangle();
        testTOUpperTriangle();

        testSubMatrix();
        testDeterminant();

    }



    private static void testSum() {

        int[][] a = {{1,3,1},{1,0,0}};
        int[][] a2 = {{0,0,5},{7,5,0}};

        System.out.println("_______________");
        Matrix sum = MultiDimentionalArrays.sum(new Matrix(a),new Matrix(a2));
        System.out.println(sum.toString());
    }
    private static void testScale() {

        int[][] b = {{1,8,-3},{4,-2,5}};
        System.out.println("_______________");
        Matrix scale = MultiDimentionalArrays.scale(new Matrix(b),2);
        System.out.println(scale.toString());
    }

    private static void testTransport(){
        int[][] c = {{1,2,3},{0,-6,7}};
        System.out.println("_______________");
        Matrix transportedMatrix = MultiDimentionalArrays.transport(new Matrix(c));
        System.out.println(transportedMatrix.toString());

    }

    private static void testMultiply(){
        int[][] e1 = {{1,2,3},{4,5,6}};
        int[][] e2 = {{7,8},{9,10},{11,12}};

        Matrix result = MultiDimentionalArrays.multiply(new Matrix(e1),new Matrix(e2));
        System.out.println(result.toString());
    }


    private static void testToDiagonal(){
        int[][]  f  = {{1,2,3},{4,5,6},{7,8,9}};
        System.out.println("_______________");
        Matrix result = MultiDimentionalArrays.toDiagonal(new Matrix(f));
        System.out.println(result.toString());
    }


    private static void testToLowerTriangle(){

        int[][]  f  = {{1,2,3},{4,5,6},{7,8,9}};
        System.out.println("_______________");
       Matrix result = MultiDimentionalArrays.toLowerTriangle(new Matrix(f));
        System.out.println(result.toString());
    }

    private static void testTOUpperTriangle(){
        int[][]  f  = {{1,2,3},{4,5,6},{7,8,9}};
        System.out.println("_______________");
        Matrix result = MultiDimentionalArrays.toUpperTriangle(new Matrix(f));
        System.out.println(result.toString());
    }

    private static void testSubMatrix(){
        int[][] f2 = {{1,2,3,4},{5,6,7,8},{9,10,11,12}};
        System.out.println("_______________");

        Matrix result = MultiDimentionalArrays.subMatrix(new Matrix(f2),2,1);
        System.out.println(result.toString());
    }

    private static void testDeterminant(){
        int[][] arr = {{8, 5, 4,4, 67},
                {687, 68, 974, 546, 5},
                {65, 65, 12, 56, 34},
                {1, 23, 35,6,7},
                {32, 123, 23,32,32}};
        System.out.println("_______________");
        int result = MultiDimentionalArrays.determinant(new Matrix(arr));
        System.out.println(result);

    }
}